%&plain
\input{header}
\begin{document}
\papertitle{Analysis of Nuclear Waste Disposal into Space}
\paperheader{Team 331: Problem A}
\vspace{-1em}\begin{center}\Large Team 331\\Problem A\end{center}
\paperdate{November 13, 2016}
\begin{paperabstract}
\input{summary}
\end{paperabstract}\vfill\pagebreak
\begin{paper}

\papersection{Introduction}

The disposal of nuclear waste is a long-term problem facing humanity.
Since nuclear waste is composed of radioactive materials of which little to no
useful energy can be extracted, it is stored in locations where the radiation
will not affect human activity.
However, most methods of storing it on Earth are prone to failure,
and can cause long-term damage to the environment.
And since there is around $\SI{2.5E8}{\kilo\gram}$ of nuclear waste on Earth,
any method to treating it needs to be robust and scalable.
In this paper, an alternate approach was considered.
The disposal of nuclear waste into space was analysed as a method.

There are two main methods of disposal of nuclear waste into space.
The first is to send it into the sun,
where it will burn up and stop being a problem.
The other is to place it into the asteroid belt,
thereby keeping it away form Earth,
and letting it decompose with time.
In this paper, these two methods are compared,
to determine which would be more efficient at getting material off the Earth.

Currently, most reactors store their spent fuel rods in pools at first.
The reason for this is because this surrounds the fuel with a lot of water.
Water is particularly effective at isolating radiation, especially in terms of
cost.

Every material has what is called a halving distance, which is the thickness
of that material that is required to absorb half of all the radiation passing
through it.
The halving distance of lead is \SI{1}{\centi\metre} while the halving
distance of water is around \SI{18}{\centi\metre}.
While more water than lead is required, it is rather easy to surround
something with a lot of water.

Apart from producing a lot of radioactivity, nuclear waste produces a lot of
heat.
Due to water's high specific heat capacity and low halving distance, water is
ideal for both shielding the radioactive waste and cooling it as it emits
heat.

After the containers are sufficiently cool, they are moved into dry casks to
be stored.
These casks are still dangerous as a breach in the shielding would allow a lot
more radioactivity to escape.
For this reason, the accumulation of nuclear waste is becoming a problem as
storing it can cause serious damage to the surrounding environment.
For this reason, sending the waste into space has been proposed as a solution.

There are many reasons for why space might not be the best receptacle for
nuclear waste.
Space travel is expensive and investing in energy that does not produce
nuclear waste is cheaper in the long run.
Rockets fail around 6\% of the time.
If a rocket filled with nuclear waste and tons of rocket fuel were to explode,
the waste would be sent flying through the atmosphere, resulting in a
catastrophic environmental disaster.

Environmental worries aside, if we decided that the only solution to the
radioactive waste problem would be to move it off-planet, then we can also
disregard the rocket failure rate as it would be a constant that we could do
nothing about.
Thus, all we need to consider is how efficiently could we move all of our
nuclear waste off planet?
What trajectories should we choose for our craft to maximize this efficiency?
Factors we will consider in choosing this trajectory are long-term safety,
cost, and waste removal rate.

\papersection{Radioactivity}

As radioactive materials decay, the rate at which they lose mass is
proportional to the amount of mass they have left.
This results in a differential equation.

\paperequation{Decay}{\frac{\d x}{\d t}=-kx}{}
\begin{paperwhere}
\paperwherevar{x}{amount of particles left}{}
\paperwherevar{t}{time}{\second}
\paperwherevar{k}{constant of proportionality}{\second}
\end{paperwhere}

Solving this equation allows us to determine the amount of particles at any
given time.

\paperequation{Amount}{N(t)=N_0e^{\text-\frac t\tau}}{}
\begin{paperwhere}
\paperwherevar{N}{amount of particles left at a given time}{}
\paperwherevar{N_0}{initial amount of particles}{}
\paperwherevar{\tau}{mean lifetime of a particle}{}
\end{paperwhere}

However, in the context of nuclear waste, which is mostly composed of depleted
uranium, the mean lifetime is on the order of billions of years.
This means that nuclear waste does not disappear quickly, or even diminish
appreciably on the timescales that we are concerned about.
As such, waiting for the nuclear waste to decay is not an option.
It has to be stored in a location far away,
remaining heavily radioactive that entire time.

\papersection{Space Travel}

When it comes to space travel, the Vis Viva equation determines the speed of the
spacecraft given a point in its orbit.

\paperequation{VisViva}{v_{r,a}=\sqrt{GM\pars{\frac2r-\frac1a}}}{}
\begin{paperwhere}
\paperwherevar{v}{the spacecraft's speed}{\metre\per\second}
\paperwherevar{G}{gravitational constant}
{\metre\cubed\per\kilo\gram\per\second\squared}
\paperwherevar{M}{the Sun's mass}{\kilo\gram}
\paperwherevar{r}{the spacecraft's distance from the sun}{\metre}
\paperwherevar{a}{the semi-major axis of the orbit}{\metre}
\end{paperwhere}

Here, the value of the gravitational constant, $G$, was taken to be
\SI{6.67408E-11}{\metre\cubed\per\kilo\gram\per\second\squared} and the Sun's
mass, $M$, was taken to be \SI{1.98855E30}{\kilo\gram}.

Since efficiency is a concern, we need to consider trajectories that save on
fuel.
The most efficient way to transfer from one orbit into another is to use a
Hohmann transfer, where the spacecraft only alter's its orbit when it is at its
apoapsis or periapsis.
To adjust from one orbit into another, \eqVisViva can be used to calculated the
required difference in velocity.

\paperequation{DeltaV}{\Delta v_{r,a1,a2}=\abs{v(r,~a_1)-v(r,~a_2)}}{}
\begin{paperwhere}
\paperwherevar{\Delta v}{the required change in speed}{\metre\per\second}
\paperwherevar{v}{the speed given by \eqVisViva}{\metre\per\second}
\paperwherevar{a_1}{the current semi-major axis}{\metre}
\paperwherevar{a_2}{the desired semi-major axis}{\metre}
\end{paperwhere}

The Tsiolkovsky Rocket Equation was also used
to compute the ratio of the initial mass of the rocket
and the final mass of the rocket.

\paperequation{Tsiolkovsky}{\Delta v=v_{e}\ln\frac{m_{0}}{m_{f}}}{}
\begin{paperwhere}
\paperwherevar{\Delta v}{the required change in velocity overall}{\metre\per\second}
\paperwherevar{v_e}{the effective exhaust velocity of the rocket}{\metre\per\second}
\paperwherevar{m_0}{the initial mass of a rocket}{\kilo\gram}
\paperwherevar{m_f}{the final mass of the rocket}{\kilo\gram}
\end{paperwhere}

\papersection{Computations}

To execute a sundive efficiently, the spacecraft needs to Hohmann transfer to an
orbit with a large apoapsis, before transferring to one with a periapsis so
small that it intersects the Sun.
The total $\Delta v$ required to execute this manoeuvre is the sum of the
$\Delta v$ required for both manoeuvres.

Thus, if the spacecraft is to execute a sundive after first flying to a distance
of $r$ from the Sun, the total $\Delta v$ can be determined using \eqDeltaV.

\paperequation{DeltaVSun}{\Delta v_{sun,r}=\Delta v_{r_e,a_e,\frac{r+a_e}2}
+\Delta v_{r,\frac{r+a_e}2,\frac{r+R}2}}{}
\begin{paperwhere}
\paperwherevar{r}{the distance from which the spacecraft sundives}{\metre}
\paperwherevar{r_e}{the distance from the Earth to the Sun}{\metre}
\paperwherevar{a_e}{the semi-major axis of Earth's orbit}{\metre}
\paperwherevar{R}{the radius of the sun}{\metre}
\end{paperwhere}

The value of $r_e$, $a_e$, and $R$ were taken to be \SI{1.49597871E11}{\metre},
\SI{1.49597871E11}{\metre}, and \SI{6.957E8}{\metre}, respectively.

This allowed the required $\Delta v$ to be calculated as a function of the
sundiving distance.

\begin{center}
\noindent
\includegraphics[width=0.85\linewidth]%,natwidth=576,natheight=432]
{precompiled_figures/sundive.pdf}
\end{center}
\papercaption{Sundive}{A plot showing the required $\Delta v$ to fly out to a
certain distance and then fly into the Sun from there.
The plot is shown as a function of said distance.
The plot has a horizontal asymptote at the escape velocity of the Sun, which is
\SI{12337.5}{\metre\per\second}.
The required $\Delta v$ becomes increasingly large for small values of $d$, and
is not shown.}

Anything sent into the sun will be immediately vaporized as the temperature of
the sun is on the order of millions of degrees while even the most resistant
materials on Earth melt at few thousand degrees.
Any risk of solar wind blowing the radioactive materials back to Earth is
mitigated by the fact that the materials would spread out over an area as they
flew away from the Sun.
By the time they will have reached Earth, the inverse square of the distance
will have gotten so small that an immeasurably small portion will make it back
to Earth.

A more pressing concern is that the container makes it back to Earth intact and
burns up in the atmosphere, scattering the radioactive waste into the air and
over the whole planet.
From \figSundive it is clear that as $r$ increases, $\Delta v$
decreases.
This means that increasing $r$ to infinity would be optimal.
However, this is equivalent to simply sending the spacecraft on an escape
trajectory.
This would guarantee that, barring some lucky and extremely improbable slingshot
around a distant black hole, the nuclear waste would never return.
Unfortunately, this would not qualify as a sundive unless the spacecraft happens
to hit a distant star, the odds of which are astronomical.

On the other hand, when nuclear waste is sent into the asteroid belt, it remains
intact in our solar system so the odds of a collision with Earth must be
computed.
First, the $\Delta v$ necessary to redirect the waste into an Earth crossing
orbit was considered.

If the waste is on a perfectly circular orbit at \SI{2.7}{\AU}, then the
required change in velocity can be calculated as a function of the angle between
the waste's initial path and its final path as well as the apoapsis of the new
orbit.

\paperequation{DeltaVColl}{\Delta v_{coll,r}=\sqrt{(v_{f,r}\cos(\phi)-v_i)^2
+(v_f\sin(\phi))^2}}{}
\begin{paperwhere}
\paperwherevar{v_i}{the initial velocity of $v_{\SI{2.7}{\AU},\SI{2.7}{\AU}}$}
{\metre\per\second}
\paperwherevar{v_f}{the final velocity of $v_{\frac{r_e+r}2,\SI{2.7}{\AU}}$}
{\metre\per\second}
\paperwherevar{\phi}{the angle between $v_i$ and $v_f$}{}
\end{paperwhere}

To calculate $\phi$, we need to take the difference between the angle of our
orbit $\theta$, and the angle of the new orbit.
The angle of the new orbit can be found as the arctangent of the slope of the
ellipse of the new orbit.
For this, we use the polar equation for an ellipse.

\paperequation{Ellipse}{r(\theta)=\frac{a(1-e^2)}{1+e\cos(\theta)}}{}
\begin{paperwhere}
\paperwherevar{\theta}{the angle from periapsis}{}
\paperwherevar{e}{the orbit's eccentricity}{}
\end{paperwhere}

This means that we can use the distance of the spacecraft from the Sun,
\SI{2.7}{\AU}, to solve for the angle by inverting the formula.

\paperequation{Angle}{\theta=\cos^{\text-1}\pars{\frac{a(1-e^2)-r}{re}}}{}

So to find $\phi$, we need to take the difference between the spacecraft's path,
which has a tangential angle of $\frac\pi2-\theta$, and the desired path, which
can be found by differentiating \eqEllipse.
We find $\theta$ using \eqAngle and this gives us a formula for $\phi$.

\paperequation{Phi}{\phi=\left|\tan^{\text-1}
\pars{\frac{\d\sqpars{r(\theta)\sin(\theta)}}
{\d\sqpars{\theta\cos(\theta)}}}
+\frac\pi2-\theta\right|}{}

All that remains is to find $a$ and $e$.
The eccentricity is always equal to the ratio of the distance between the foci
to the major-axis.
Thus we can subtract the periapsis, which is \SI{1}{\AU}, from $a$ and divide
the result by $a$ to obtain $e$.
Meanwhile, $a$ is just the average of the periapsis, \SI{1}{\AU}, and the
apoapsis, which we vary.

Thus, the necessary imparted energy to put our container on an earth-crossing
course was determined as a function of the new orbit's apoapsis.

\begin{center}
\noindent
\includegraphics[width=0.85\linewidth]%,natwidth=576,natheight=432]
{precompiled_figures/collision.pdf}
\end{center}
\papercaption{Collision}{A plot showing the required $\Delta v$ to send the
nuclear waste onto an Earth crossing path from a circular orbit at
\SI{2.7}{\AU}.
This is expressed as a function of the semi-major axis of the orbit that the
waste is knocked into.
The plot has no values for small values of $a$ as an orbit with a sufficiently
small semi-major axis cannot intersect the asteroid belt.}

\papersection{Designing the Container}

When designing the container,
there were two considerations:
It had to not damage the equipment,
and it had to not harm the workers placing
the container on the rocket.

The latter requirement turned out to be harsher.
\SI{50}{\milli\sievert} is the yearly limit on radiation
that a worker can be exposed to.
Meanwhile, radioactive waste exposure within several meters
is usually fatal.
As such, let us suppose that radioactive waste exposure provides
\SI{5}{\sievert} per day.
It then provides \SI{1825}{\sievert} of radiation in a single year.
As a result, the radiation had to be halved
15 times to become safe.

The container was to be made of lead,
which has a halving distance of $\SI{1}{\centi\metre}$.
As a result, the container had to have walls
at least $\SI{15}{\centi\metre}$ thick.
For additional safety precautions,
the walls were extended to be $\SI{20}{\centi\metre}$ thick.

\papersection{Asteroid Belt Stability of Orbit}
% Collision Probability: 4.0350266701033515e-17
% Average collision energy: 14059772096.37372
% Average E per unit time: 5.673155538444286e-07
% Average E in 1Ky: 17890.8633060379
% Average DV in 1Ky: 2.114879624590836
% Probability of a collision in 1Ky: 1.272485201098128e-06
% Average E in 1 billion y: 17890863306.0379
% Average DV in 1 billion y: 2114.8796245908356
% Probability of a collision in 1by: 0.7198656615476937

\begin{center}
\noindent
\includegraphics[width=0.85\linewidth]%,natwidth=576,natheight=432]
{precompiled_figures/abeltorbit.pdf}
\end{center}
\papercaption{ABelt}{A plot of a potential flight plan
and eventual orbit that nuclear waste could take on if
launched into the asteroid belt to a circular orbit at \SI{2.7}{\AU}.
The solid lines are the orbits of the 5 innermost planets.
The dashed lines are the boundaries of the asteroid belt at \SI{2.2}{\AU} and
\SI{3.2}{\AU}.}

Now that the necessary $\Delta v$ for a collision that would put Earth at risk
had been computed, the odds of such an event actually occurring were calculated.
To do this, it was necessary to obtain the orbital information of the asteroids
that make up the asteroid belt.
This was done with a simple program that used the \texttt{curl}
utility to download entries from the NASA JPL Small Body Database.
This utility was used to load information on over \num{50000} asteroids.

A Python program was written to analyze the retrieved data.
The data for each asteroid had the physical parameters extracted.
These were written to a central database,
from which readings could be taken.

\begin{center}
\noindent
\includegraphics[width=0.85\linewidth]%,natwidth=576,natheight=432]
{precompiled_figures/assvol.pdf}
\end{center}
\papercaption{AssVols}{
A plot displaying the largest asteroids in the asteroid belt.
The largest asteroid is plotted furthest left,
while smaller asteroids are further to the right.
The $x$ axis indicates the ordering statistic of the asteroids,
while the $y$ axis indicates the size of the asteroids.}

The data was then processed by another program
to determine the masses and volumes of the asteroids.
First, the program computed an approximate volume for each asteroid based its
the diameter.
Asteroids were modeled as spheres, as not enough information was provided to
make an alternate assumption.
Subsequently, using the asteroids with available masses,
the average density of the asteroids was computed to be
\SI{3400}{\kilo\gram\per\metre\cubed}.
This density and the volumes were used
to determine the mass for the remaining asteroids.

As a model, the asteroid belt was assumed to span the range from $\SI{2.2}{\AU}$
to $\SI{3.2}{\AU}$.
In other words, it was assumed, for the purpose of modeling,
that all main-belt asteroids
were between $\SI{2.2}{\AU}$ and $\SI{3.2}{\AU}$
away from the sun at all times.
The possible range of velocities was thus computed using \eqVisViva,
by finding the minimal and maximum possible velocities
for orbits in that range.
These were found to be \SI{15029.9}{\metre\per\second} and
\SI{21861.7}{\metre\per\second}, respectively.

The velocities were assumed to be distributed
according to a normal distribution between the possible extremes of velocity.
It was assumed that $3\sigma$ of asteroids had velocities located in this range,
so that nearly all asteroids lay in the intended range.
There are a number of asteroids that have very eccentric orbits and that do not
stay confined to the range described above.
There are very few of these and their presence does not change the resulting
values, thus, the statistically significant margin of 3$\sigma$ was chosen.
This is another way of stating that only 1 out of every 1000 main belt asteroids
is outside of the indicated range.

The nuclear waste was assumed to be put into orbit
at a distance of $\SI{2.7}{\AU}$ from the sun.
Given this and the above distribution,
the average difference in speed between the waste
and an asteroid in the main belt was computed.
This was used to calculate the average amount of energy
that would be imparted onto the waste in case of a collision.

To compute the probability of a collision occurring,
the following approach was used.
The asteroids were examined in the frame of reference of the waste in intervals
of time $\Delta t$.
In that time interval,
the probability of a collision was equal
to the volume newly swept out by each asteroid relative to the waste,
divided by the total volume of the belt.
The volume swept out by an asteroid was determined to be equal to
its cross-sectional area multiplied by the distance it traveled in that time,
which was in turn determined to be
the speed of the asteroid relative to the waste, multiplied by $\Delta t$.
This volume was calculated for each asteroid for which data was known, and then
extrapolated for the entire asteroid belt.
In unit time, the probability of a collision was thus computed to be
the above probability divided by $\Delta t$.
And thus, it was computed to be

\paperequation{PColl}{P_{coll}
=\frac{\sum A_kv_k}{V}}{}
\begin{paperwhere}
\paperwherevar{P_{coll}}{probability of a collision in unit time}{}
\paperwherevar{A_k}{the cross-sectional area of the $k^\text{th}$ asteroid}
{\metre\squared}
\paperwherevar{v_k}{velocity of the $k^\text{th}$ asteroid}{\metre\per\second}
\paperwherevar{V}{volume of the main-belt}{\metre\cubed}
\end{paperwhere}

Thus, the probability of a collision in a unit time was computed.
By modeling collisions as Poisson process,
the probability of a collision in a billion years could be computed.
By using an exponential distribution,
it was found that there was a $0.0001\%$ chance
of a collision occurring in the first thousand years.

By combining the probability of a collision in unit time
with the average speed difference computed above,
the expected amount of energy
imparted onto the nuclear waste in unit time could be computed.
It was computed to be $\SI{5.67E-7}{\joule\per\second}$.
From this, the expected amount of energy imparted
over a billion years could be computed.
This then gave the expected $\Delta v$
imparted due to collisions over a billion years.
This was found to be $\SI{2115}{\metre\per\second}$.
As the required $\Delta v$ to send it on an Earth-crossing path
was at least $\SI{4000}{\metre\per\second}$,
it was decided that the danger to Earth
from putting the container in the asteroid belt was negligible.

\papersection{Sun Optimality of Orbit}

\begin{center}
\noindent
\includegraphics[width=0.85\linewidth]%,natwidth=576,natheight=432]
{precompiled_figures/sundiveorbit.pdf}
\end{center}
\papercaption{SunDive}{A plot of a potential flight plan
that nuclear waste to be deorbited into the sun could take, reaching
\SI{10}{\AU} at its peak.
The solid lines are the orbits of the 5 innermost planets.
The dashed lines are the boundaries of the asteroid belt at \SI{2.2}{\AU} and
\SI{3.2}{\AU}.}

For the solar disposal,
a different issue was considered:
How much $\Delta v$ needed to be generated to land the waste in the sun?
The more $\Delta v$ it takes,
the more fuel needs to be packed,
which makes it more expensive to dispose of the waste.

The initial approach was to just burn near Earth,
to alter the orbit of the nuclear waste into crossing the Sun.
However, an alternate approach was proposed,
where an initial burn was used to put the waste
into an orbit heading to a distance $d$ away from the sun,
where a second burn would be performed
that would put the waste on a sun-crossing path.

Using \eqVisViva,
it was found that as $d$ increased,
the required $\Delta v$ throughout the journey decreased.
Thus, it was found to be more cost-effective to first build up speed and
go far away from the sun,
where a small burn would allow the waste to just fall into the sun.

This also led to the consideration of another disposal method:
Another way of disposing the waste would be to put in on an escape trajectory.
This was found to require less $\Delta v$ than any solar disposal.

\begin{center}
\noindent
\includegraphics[width=0.85\linewidth]%,natwidth=576,natheight=432]
{precompiled_figures/escapeorbit.pdf}
\end{center}
\papercaption{HypeEsc}{A plot of a potential flight plan
that nuclear waste to be ejected from the solar system could take.
The solid lines are the orbits of the 5 innermost planets.
The dashed lines are the boundaries of the asteroid belt at \SI{2.2}{\AU} and
\SI{3.2}{\AU}.}

\papersection{Comparison}

Using the calculations in the earlier sections,
the required $\Delta v$ for each method was computed.
The exact $\Delta v$ values needed for some methods are listed below.

\noindent
\begin{papertable}{|[outer]X[-1,r]|[inner]l|[outer]}
\paperoline
Method&$\Delta v$ Required\\
\paperiline
Decelerating to fall directly into the sun
&\SI{26919.4}{\metre\per\second}\\
\paperiline
Decelerating to fall into the sun after going out to $\SI{10}{\AU}$
&\SI{14106.2}{\metre\per\second}\\
\paperiline
Accelerating to escape velocity out of the Solar System
&\SI{12337.5}{\metre\per\second}\\
\paperiline
Entering orbit in the Asteroid Belt at $\SI{2.7}{\AU}$
&\SI{10997.4}{\metre\per\second}\\
\paperiline
Entering orbit in the Asteroid Belt at $\SI{2.2}{\AU}$
&\SI{9346.67}{\metre\per\second}\\
\paperoline
\end{papertable}

As can be seen in the table,
the cheapest method,
requiring the least $\Delta v$,
is disposing waste into the asteroid belt.
This reduced $\Delta v$ allows for a decreased amount of fuel required,
allowing for more nuclear waste to be disposed of in a single rocket.
% Hyperbolic escape: 12337.5.
% Falling into the sun directly: 26919.4
% Falling into sun after 10 AU: 14106.2
% Getting into 2.8 AU: 11259.2

% 28.1473520521
% 1.1570244946231363,3002578.042050725,3892628.9668077235
% 166.02700573582075,3002413.172069484,27125.8265373424
% 329.5358418828084,3002249.663233337,13665.810884545675
% 29528.109960491445,2973051.0891147284,151.0281775446853
% 558.2738080293572,3002020.92526719,8065.990779320227
% 1050.3808370859745,3001528.8182381336,4286.343646412776
Using the above $\Delta v$,
several rockets were reviewed for their efficiency
in getting the nuclear waste to its goals.
The Saturn V was used as the rocket to deliver material.
By using \eqTsiolkovsky,
the payload capacity to be delivered a given location was computed.
The maximum amount of waste a single rocket could deliver to the sun,
after first heading out to $\SI{10}{\AU}$ was found to be
$\SI{166}{\kilo\gram}$.
The maximum amount of waste a single rocket could deliver to the asteroid belt
at $\SI{2.7}{\AU}$ was found to be $\SI{558}{\kilo\gram}$.

The total amount of radioactive waste on Earth was sourced as
$\SI{2.5E8}{\kilo\gram}$.
Thus, disposal into the sun would require
around \num{15E5} rocket launches,
while disposal into the asteroid belt would require
\num{4.5E5} rocket launches.
As this number is much less for the asteroid belt,
the asteroid belt was found to be a significantly cheaper method of disposal.

However, sending nuclear waste to the asteroid belt is more dangerous to Earth.
Waste that is discarded into the sun is rapidly destroyed by solar heat.
However, waste that is stored in the asteroid belt decomposes slowly.
Although the probability of a collision
was computed to be $0.0001\%$ in the thousand years,
that does not make it impossible.
And in the case of a collision,
this would leave free-floating nuclear waste in the asteroid belt.
However, the more than $\SI{1}{\AU}$ between Earth and the asteroid belt
means that even if there is a collision,
the nuclear waste has low odds of returning to Earth to cause problems.
As such, this method is sufficiently safe that any risks can be ignored.

\papersection{Conclusion}

Our method restricts the set of possible solutions to solar disposal and storage
in the asteroid belt.
However, there are many more practical solutions.
The waste could be ejected from the solar system,
or it could have been gradually decelerated into the sun
using a slow but consistent source of deceleration such as an ion engine.

Additionally, this approach
is that the fuel requirements for the rockets are on the high side.
Calculating the exact fuel requirements
is not an analytically solvable problem and would require taking into account
loss of stages and other such mechanisms.
To simplify the calculation,
it was assumed that the Tsiolkovsky rocket equation entirely determined the
rocket speed, and that all non-payload material in the rocket was discarded at a
constant rate.
This overestimated the amount of payload that could be delivered in a single
launch, and underestimated the fuel needed.
However, any comparisons between the methods still behaved the same way.
If one method would require more fuel under one calculation,
it would require more under the other.
As such, any conclusions as to the efficiency of either method still held,
even with the assumption to simplify the calculations.

Lastly, the odds of a launch failure still outweigh the odds of a container
coming back to Earth.
Furthermore, the costs associated with cleaning the environmental disaster that
would result from a failed rocket launch with nuclear waste on board far
outweighs the cost of moving the waste off-planet.
Thus, since this cost was the same in all approaches in terms of trajectory, it
was disregarded.

This method did not make any approximations regarding space travel and
fully calculated all the required $\Delta v$'s for elliptical orbits.
This approach fully solved the completely infeasible problem of moving all of
Earth's nuclear waste off-planet.

The final verdict on the issue is that it is far more efficient to move the fuel
into the asteroid belt, but would still require \SI{4.5E5}{} Saturn V rockets to
complete the task.
Since in 1970, each Saturn V launch would cost NASA \$185 million, then in
today's money, this would cost over \$500 trillion dollars.
This is several times more than the annual gross domestic product of the entire
world, so the authors of this paper strongly urge the consideration of a
different method.

\papersection{Sources}
\url{http://www.wired.co.uk/article/into-eternity-nuclear-waste-finland}\\
\url{https://en.wikipedia.org/wiki/Sievert}\\
\url{https://en.wikipedia.org/wiki/Saturn_V}
\url{http://www.space.com/17137-how-hot-is-the-sun.html}\\
\url{https://whatisnuclear.com/articles/waste.html}
\url{http://www.astro.cornell.edu/~randerson/Inreach}
\url{http://nssdc.gsfc.nasa.gov/planetary/factsheet/}\\
\url{http://ssd.jpl.nasa.gov/sbdb.cgi}
\end{paper}

\pagebreak
\lstinputlisting[style=paperpy]{code/deltav.py}
\pagebreak
\lstinputlisting[style=paperpy]{code/graph_gen.py}
\pagebreak
\lstinputlisting[style=paperpy]{code/mv_extractor.py}
\pagebreak
\lstinputlisting[style=paperpy]{code/param_scrape.py}
\pagebreak
\lstinputlisting[style=paperpy]{code/plot_mv.py}
\pagebreak
\lstinputlisting[style=paperpy]{code/probability.py}
\pagebreak
\lstinputlisting[style=paperbash]{code/load.sh}
\pagebreak
\lstinputlisting[style=paperbash]{code/scrape.sh}
\end{document}
