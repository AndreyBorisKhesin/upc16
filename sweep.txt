Suppose that our spaceship "jumps" every dt time interval.
Then, our probability of being hit each dt interval
is the new volume swept out by all the asteroids.
This can be tranformed into cross-sectional area times velocity times dt.

.'. P(hit during [t,t+dt]) = (sum A*v*dt)/V
Note that P(hit during [t,t+1]) = (sum A*v*dt)/dt/V
= (sum A*v)/V

Thus, lambda = (sum A*v)/V,
and we can use a Poisson distribution.


V ~ Normal((v_min+v_max)/2,(v_max-v_min)/6)
m = (v_min+v_max)/2
s = (v_max-v_min)/6
D = |V-v|
Expected value of D?

<D> = int_{-oo}^{oo} Dp(D)
= int_{-oo}^{oo}|V-v|p(V)
= int_{-oo}^{oo}|x-v|1/(s*rt(2pi))e^(-1/2((x-m)/s)^2)d x
= int_{-oo}^{v}(v-x)1/(s*rt(2pi))e^(-1/2((x-m)/s)^2)d x
    + int_{v}^{oo}(x-v)1/(s*rt(2pi))e^(-1/2((x-m)/s)^2)d x
= 1/(s*rt(2pi)) * [int_{-oo}^{v}ve^{-1/2((x-m)/s)^2}d x - int_{-oo}^{v}xe^{-1/2((x-m)/s)^2}dx
    + int_{v}^{oo}xe^{-1/2((x-m)/s)^2}d x - int_{v}^{oo}ve^{-1/2((x-m)/s)^2}dx]
y = (x-m)/s
x = sy+m
dx = sdy
<D> = 1/(s*rt(2pi)) * [int_{-oo}^{v}ve^{-1/2((x-m)/s)^2}d x - int_{-oo}^{v}xe^{-1/2((x-m)/s)^2}dx
    + int_{v}^{oo}xe^{-1/2((x-m)/s)^2}d x - int_{v}^{oo}ve^{-1/2((x-m)/s)^2}dx]
= 1/(rt(2pi)) * [int_{-oo}^{v-m/s}ve^{-1/2y^2}dy - int_{-oo}^{v-m/s}(sy+m)e^{-1/2y^2}dy
    + int_{v-m/s}^{oo}(sy+m)e^{-1/2y^2}dy - int_{v-m/s}^{oo}ve^{-1/2y^2}dy]
= 1/(rt(2pi)) * [int_{-oo}^{v-m/s}(v-m)e^{-1/2y^2}dy - sint_{-oo}^{v-m/s}ye^{-1/2y^2}dy
    + sint_{v-m/s}^{oo}ye^{-1/2y^2}dy - int_{v-m/s}^{oo}(v-m)e^{-1/2y^2}dy]
= 1/(rt(2pi)) * [rt(2pi)(v-m)int_{-oo}^{v-m/s}1/rt(2pi)e^{-1/2y^2}dy - sint_{-oo}^{v-m/s}ye^{-1/2y^2}dy
    + sint_{v-m/s}^{oo}ye^{-1/2y^2}dy - rt(2pi)(v-m)int_{v-m/s}^{oo}1/rt(2pi)e^{-1/2y^2}dy]
= 1/(rt(2pi)) * [rt(2pi)(v-m)Z(v-m/s) - sint_{-oo}^{v-m/s}ye^{-1/2y^2}dy
    + sint_{v-m/s}^{oo}ye^{-1/2y^2}dy - rt(2pi)(v-m)(1-Z(v-m/s))]
= 1/(rt(2pi)) * [rt(2pi)(v-m)Z(v-m/s) - s(-e^{-1/2((v-m)/s)^2})
    + s(e^{-1/2((v-m)/s)^2}) - rt(2pi)(v-m)(1-Z(v-m/s))]
= 2(v-m)Z((v-m)/s) + 2s/rt(2pi)*e^{-1/2((v-m)/s)^2} - (v-m)
