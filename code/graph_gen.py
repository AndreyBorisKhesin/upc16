import numpy as np
import matplotlib.pyplot as plt
import sys

sunm = 1.98855E30
sunr = 695700000
G = 6.67408E-11
au = 149597871000
v = lambda a, r: np.sqrt(G*sunm*(2/r-1/a))
dv = lambda d: (
    np.abs(v((d+au)/2, au) - v(au, au)) +
    np.abs(v((d+sunr)/2, d) - v((d+au)/2, d)))
d = np.linspace(0.1, 20, 10000)
plt.plot(d, dv(d*au))
plt.axis((0, 10, 0, 50000))
plt.title("Necessary $\\Delta v$ for solar deorbiting")
plt.ylabel("$\\Delta v$ (ms$^{-1}$)")
plt.xlabel("$d$ (AU)")
plt.savefig(sys.argv[1])
plt.clf()

# Plot[Norm@{vf Cos@\[Theta] - vi, vf Sin@\[Theta]} /. {vi ->
# v[2.7 au, 2.7 au],
# vf -> v[(au + x)/2,
# 2.7 au], \[Theta] -> ((1/
# 2 Abs[\[Pi] - 2 \[Theta] -
# 2 ArcTan[(e +
# Cos[\[Theta]]) Csc[\[Theta]]]] /. {\[Theta] ->
# ArcCos@((a (-d + a (1 - (a - p)^2/a^2)))/(d (a - p))),
# e -> (a - p)/p}) /. {p -> au, d -> 2.7 au,
#   a -> (au + x)/2})}, {x, 2.7 au, 10 au},
#   PlotRange -> {0, 40000}]
d = 2.7*au
p = au
a = lambda x: (au + x)/2
e = lambda x: (a(x)-p)/a(x)
th = lambda x: (np.arccos(
    (a(x)*a(x) - a(x)*d - (a(x)-p)*(a(x)-p)) /
    (d*(a(x)-p))))
dth = lambda x: 0.5 * np.absolute(
    np.pi - 2*th(x) - 2 * np.arctan(
        (e(x) + np.cos(th(x))) / np.sin(th(x))))
vf = lambda x: v((p+x)/2, d)
vi = v(d, d)
dv2 = lambda x: np.sqrt(
    (vf(x)*np.cos(dth(x))-vi) * (vf(x)*np.cos(dth(x))-vi) +
    (vf(x)*np.sin(dth(x))) * (vf(x)*np.sin(dth(x))))
x = np.linspace(2.7, 10, 1000)
plt.plot(x, dv2(x*au))
plt.axis((0, 10, 0, 20000))
plt.title("Necessary $\\Delta v$ to enter an Earth-crossing orbit")
plt.ylabel("$\\Delta v$ (ms$^{-1}$)")
plt.xlabel("$d$ (AU)")
plt.savefig(sys.argv[2])
