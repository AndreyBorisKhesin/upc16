import numpy as np

g = 9.81

# Saturn V
Isp = 263
m_s = 2970000
m_w1 = 140000
dv_1 = 7909.3
dv_e = 11185.4

M = m_w1*(np.exp(dv_1/g/Isp))
m_w = lambda dv: M/(np.exp(dv/g/Isp))
m_f = lambda dv: M - m_w(dv)
p = lambda dv: 1.5*m_f(dv)/m_w(dv)
m_str = lambda dv: "{},{},{}".format(
    m_w(dv),
    m_f(dv),
    p(dv))

max_dv = g*Isp*np.log(M/m_s)

print(max_dv)
print(m_str(26919.4+dv_e))
print(m_str(14106.2+dv_e))
print(m_str(12337.5+dv_e))
print(m_str(11924.6))
print(m_str(10977.4+dv_e))
print(m_str(9346.67+dv_e))
