#!/usr/bin/python
import matplotlib.pyplot as plt
import numpy as np
import re
import sys

line_patt = re.compile(r'([0-9.e+]*),([0-9.e+]*),([0-9.e+]*)')
mvfile = open(sys.argv[1], 'r')
masses = []
volumes = []
for line in mvfile:
    line_match = line_patt.match(line)
    if line_match is not None:
        masses.append(float(line_match.group(2)))
        volumes.append(float(line_match.group(3)))
masses.sort(reverse=True)
plt.semilogy(np.arange(len(masses)), masses)
plt.title("Masses of " + str(len(masses)) + " heaviest main-belt asteroids")
plt.xlabel("$n$'th heaviest asteroid")
plt.ylabel("$m$")
plt.savefig("graphs/assmass.pdf")
plt.clf()
volumes.sort(reverse=True)
plt.semilogy(np.arange(len(volumes)), volumes)
plt.title("Volumes of " + str(len(volumes)) + " largest main-belt asteroids")
plt.xlabel("$n$'th largest asteroid")
plt.ylabel("$V$")
plt.savefig("graphs/assvol.pdf")
