import numpy as np
import scipy.stats as sps
import sys

mvfile = sys.argv[1]

n, m, V = np.loadtxt(
    mvfile,
    unpack=True,
    skiprows=1,
    delimiter=',',
    )

A = np.power(V / np.pi * 0.75, 2.0/3.0) * np.pi

au = 149597871000
r_max = 3.2*au
r_min = 2.2*au

V = np.pi * np.pi * (r_max-r_min)*(r_max-r_min)*(r_max+r_min) / 4

v_max = 21861.7
v = 18126.7
v_min = 16650.5

s = (v_max-v_min)/6
m = (v_min+v_max)/2

y = (v-m)/s

Z1 = sps.norm.cdf(y)
Z2 = np.exp(-y*y/2)
D = 2*(v-m)*Z1 + 2*s/np.sqrt(2*np.pi)*Z2 - (v-m)

N = 10000000

P_coll = np.sum(A)*D/V*N/len(n)
print("Collision Probability: {}".format(P_coll))
print("Average collision energy: {}".format(np.mean(m*D*D/2)))
print("Average E per unit time: {}".format(np.sum((A*D/V)*N/len(n)*(m*D*D/2))))
P_s = np.sum((A*D/V)*N/len(n)*(m*D*D/2))
# Average amount of energy impacted per second
print("Average E in 1Ky: {}".format(P_s * 60 * 60 * 24 * 365 * 1E3))
print("Average DV in 1Ky: {}".format(
    np.sqrt(P_s * 60 * 60 * 24 * 365 * 1E3 * 2 / 8000)))
print("Probability of a collision in 1Ky: {}".format(
    1-np.exp(-P_coll*60*60*24*365*1E3)))
print("Average E in 1 billion y: {}".format(P_s * 60 * 60 * 24 * 365 * 1E9))
print("Average DV in 1 billion y: {}".format(
    np.sqrt(P_s * 60 * 60 * 24 * 365 * 1E9 * 2 / 8000)))
print("Probability of a collision in 1by: {}".format(
    1-np.exp(-P_coll*60*60*24*365*1E9)))
