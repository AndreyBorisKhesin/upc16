#!/usr/bin/python
import math
import re
import sys

mass_patt = re.compile(r'(\d*),[^,]*,GM,([^,]*),([^,]*)')
dia_patt = re.compile(r'(\d*),[^,]*,diameter,([^,]*),([^,]*)')
# m is in km^3/s^2
# d is in km
# G is in m^3/kg/s^2
G = 6.67408E-11
aggfile = open(sys.argv[1], 'r')
data = {}
for line in aggfile:
    mass_match = mass_patt.search(line)
    dia_match = dia_patt.search(line)
    if mass_match is not None:
        if mass_match.group(1) not in data:
            data[mass_match.group(1)] = {}
        data[mass_match.group(1)]['GM'] = mass_match.group(2)
    if dia_match is not None:
        if dia_match.group(1) not in data:
            data[dia_match.group(1)] = {}
        data[dia_match.group(1)]['diameter'] = dia_match.group(2)
n = 0
total_rho = 0
for asteroid in data:
    if 'GM' in data[asteroid]:
        data[asteroid]['mass'] = float(data[asteroid]['GM']) / G * 1E9
    if 'diameter' in data[asteroid]:
        volume = (
            1.0/6.0 * math.pi *
            float(data[asteroid]['diameter']) *
            float(data[asteroid]['diameter']) *
            float(data[asteroid]['diameter']))
        data[asteroid]['volume'] = volume * 1E9
        if 'GM' in data[asteroid]:
            total_rho += float(data[asteroid]['GM']) / G / volume
            n += 1
rho = total_rho/n
print('n,m,V,{}'.format(rho))
for asteroid in data:
    if 'volume' in data[asteroid] and 'mass' not in data[asteroid]:
        data[asteroid]['mass'] = rho * data[asteroid]['volume']
    if 'volume' in data[asteroid] and 'mass' in data[asteroid]:
        print("{},{},{}".format(
            asteroid,
            data[asteroid]['mass'],
            data[asteroid]['volume']))
