import re
import sys

num = re.compile(r'\d+').search(sys.argv[1]).group(0)
htmlfile = open(sys.argv[1], 'r')
start_table = False
end_table = False
k = 0
n = -1
attr_pattr = re.compile(r'<font[^>]*>([^<]*)<')
results = []
mb = False
for line in htmlfile:
    if k > 0:
        k -= 1
        results[n].append(attr_pattr.search(line).group(1))
    if start_table and "<tr>" in line:
        k = 5
        n += 1
        results.append([])
    if start_table and "/table" in line:
        break
    if '"phys_par' in line:
        start_table = True
    if "Main-belt" in line:
        mb = True
if mb:
    for result in results:
        print "{},{}".format(num, ','.join(result))
