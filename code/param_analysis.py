#!/usr/bin/python
import re
import sys

param = sys.argv[1]
line_match = re.compile(r'(\d*),[^,]*,'+param+r',([^,]*),([^,]*)')
aggfile = open(sys.argv[2], 'r')
for line in aggfile:
    match = line_match.search(line)
    if match is not None:
        print("{},{} {}".format(
            match.group(1),
            match.group(2),
            match.group(3)))
    pass
