for i in `seq 1 $1`
do
    if [ -f data/raw/$i.html ]
    then
        python code/param_scrape.py data/raw/$i.html >> $2
    fi
done
